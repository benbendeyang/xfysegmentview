# XFYSegmentView

[![CI Status](https://img.shields.io/travis/leonazhu/XFYSegmentView.svg?style=flat)](https://travis-ci.org/leonazhu/XFYSegmentView)
[![Version](https://img.shields.io/cocoapods/v/XFYSegmentView.svg?style=flat)](https://cocoapods.org/pods/XFYSegmentView)
[![License](https://img.shields.io/cocoapods/l/XFYSegmentView.svg?style=flat)](https://cocoapods.org/pods/XFYSegmentView)
[![Platform](https://img.shields.io/cocoapods/p/XFYSegmentView.svg?style=flat)](https://cocoapods.org/pods/XFYSegmentView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XFYSegmentView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XFYSegmentView'
```

## Author

leonazhu, 412038671@qq.com

## License

XFYSegmentView is available under the MIT license. See the LICENSE file for more info.
