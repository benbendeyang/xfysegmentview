//
//  SegmentGroupView.swift
//  Pods-XFYSegmentView_Example
//
//  Created by 🐑 on 2019/3/25.
//
//  Segment组合视图

import UIKit

open class SegmentGroupView: UIView {
    
    open lazy var segmentView: SegmentView = {
        let segmentView = SegmentView()
        segmentView.didSelectAtIndex = { [weak self] index in
            guard let `self` = self else { return }
            let lastIndex = Int(self.mainScrollView.contentOffset.x / self.mainScrollView.bounds.width)
            guard lastIndex != index else { return }
            self.mainScrollView.setContentOffset(CGPoint(x: self.mainScrollView.bounds.width * CGFloat(index), y: 0), animated: false)
            let animationType: CATransitionSubtype = (lastIndex > index) ? .fromLeft : .fromRight
            self.animationPush(type: animationType)
            self.didSelectAtIndex?(index)
        }
        segmentView.selectIndexChange = { [weak self] index in
            guard let `self` = self else { return }
            self.selectIndexChange?(index)
        }
        return segmentView
    }()
    open lazy var mainScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        return scrollView
    }()
    private var isDragging: Bool = false
    public var segmentViewHeight: CGFloat = 44 {
        didSet {
            refreshView()
        }
    }
    public var items: [(title: String, view: UIView)] = [] {
        didSet {
            let titles = items.compactMap { title, _ -> String in
                return title
            }
            segmentView.setItem(titles: titles)
            mainScrollView.subviews.forEach { subview in
                subview.removeFromSuperview()
            }
            items.forEach { _, view in
                mainScrollView.addSubview(view)
            }
            refreshView()
        }
    }
    open var didSelectAtIndex: ((_ index: Int) -> Void)?
    open var selectIndexChange: ((_ index: Int) -> Void)?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        refreshView()
    }
}

// MARK: - 公共
public extension SegmentGroupView {
    
    func setupStyle(font: UIFont? = nil, normalColor: UIColor? = nil, selectedColor: UIColor? = nil, itemMode: SegmentItemMode? = nil, lineMode: SegmentLineMode? = nil, segmentViewHeight: CGFloat? = nil) {
        if let height = segmentViewHeight {
            self.segmentViewHeight = height
        }
        segmentView.setupStyle(font: font, normalColor: normalColor, selectedColor: selectedColor, itemMode: itemMode, lineMode: lineMode)
    }
    
    func select(at index: Int, animate: Bool = true) {
        segmentView.select(at: index, animate: animate)
    }
}

// MARK: - 私有
private extension SegmentGroupView {
    
    func initView() {
        addSubview(segmentView)
        addSubview(mainScrollView)
        backgroundColor = .white
        refreshView()
    }
    
    func refreshView() {
        let width = bounds.width
        segmentView.frame = CGRect(x: 0, y: 0, width: width, height: segmentViewHeight)
        mainScrollView.frame = CGRect(x: 0, y: segmentViewHeight, width: width, height: bounds.height - segmentViewHeight)
        items.enumerated().forEach { index, item in
            item.view.frame = CGRect(x: CGFloat(index) * width, y: 0, width: width, height: mainScrollView.bounds.height)
        }
        mainScrollView.contentSize = CGSize(width: CGFloat(items.count) * width, height: mainScrollView.bounds.height)
    }
    
    func animationPush(type: CATransitionSubtype) {
        let animation: CATransition = CATransition()
        animation.duration = 0.3
        animation.type = .push
        animation.subtype = type
        mainScrollView.layer.add(animation, forKey: nil)
    }
}

// MARK: - 协议
extension SegmentGroupView: UIScrollViewDelegate {
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard isDragging else { return }
        let index = Int(scrollView.contentOffset.x / scrollView.bounds.width + 0.5)
        segmentView.setIndexInScrolling(index: index)
        let offsetX = scrollView.contentOffset.x
        let referOffsetX = scrollView.bounds.width * CGFloat(segmentView.selectIndex)
        segmentView.itemOffset(ratio: CGFloat((offsetX - referOffsetX)/scrollView.bounds.width))
    }
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isDragging = true
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        isDragging = false
    }
}

