//
//  SegmentViewCell.swift
//  XFYSegmentView_Example
//
//  Created by 🐑 on 2019/3/22.
//  Copyright © 2019 CocoaPods. All rights reserved.
//
//  Segment单元视图

import UIKit

class SegmentViewCell: UICollectionViewCell {
    
    private let contentLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentLabel.frame = bounds
        contentLabel.textAlignment = .center
        addSubview(contentLabel)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        contentLabel.frame = bounds
    }
    
    func show(content: String? = nil, font: UIFont? = nil, color: UIColor? = nil) {
        if let content = content {
            contentLabel.text = content
        }
        if let font = font {
            contentLabel.font = font
        }
        if let color = color {
            contentLabel.textColor = color
        }
    }
}
