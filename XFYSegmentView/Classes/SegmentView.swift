//
//  SegmentView.swift
//  XFYSegmentView_Example
//
//  Created by 🐑 on 2019/3/22.
//  Copyright © 2019 CocoaPods. All rights reserved.
//
//  分段选择器

import UIKit

public enum SegmentItemMode {
    case divide
    case arrange
}
public enum SegmentLineMode {
    case full
    case fixed
}

open class SegmentView: UIView {
    
    private lazy var mainCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = UIScreen.main.bounds.size
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(SegmentViewCell.self, forCellWithReuseIdentifier: "SegmentViewCell")
        return collectionView
    }()
    private var lineView: UIView = UIView()
    open lazy var dividerLine: UIView = {
        let line = UIView()
        line.backgroundColor = #colorLiteral(red: 0.8666666667, green: 0.8666666667, blue: 0.8666666667, alpha: 1)
        return line
    }()
    private var lineMode: SegmentLineMode = .full {
        didSet {
            switch lineMode {
            case .full:
                lineView.layer.cornerRadius = 1
            case .fixed:
                lineView.layer.cornerRadius = 1.5
            }
        }
    }
    private var itemMode: SegmentItemMode = .divide
    private var titles: [String] = []
    public private(set) var selectIndex: Int = 0 {
        didSet {
            selectIndexChange?(selectIndex)
        }
    }
    open var fixedLineWidth: CGFloat = 25
    open var itemFont: UIFont = .systemFont(ofSize: 15)
    open var itemNormalColor: UIColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
    open var itemSelectedColor: UIColor = #colorLiteral(red: 1, green: 0.2705882353, blue: 0, alpha: 1) {
        didSet {
            lineView.backgroundColor = itemSelectedColor
        }
    }
    open var itemSpace: CGFloat = 12
    open var didSelectAtIndex: ((_ index: Int) -> Void)?
    open var selectIndexChange: ((_ index: Int) -> Void)?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        mainCollectionView.frame = bounds
        dividerLine.frame = CGRect(x: 0, y: bounds.height - 0.5, width: bounds.width, height: 0.5)
    }
}

// MARK: - 公共
public extension SegmentView {
    
    func setItem(titles: [String], selectIndex: Int = 0) {
        self.titles = titles
        select(at: selectIndex, animate: false)
    }
    
    func setupStyle(font: UIFont? = nil, normalColor: UIColor? = nil, selectedColor: UIColor? = nil, itemMode: SegmentItemMode? = nil, lineMode: SegmentLineMode? = nil) {
        if let font = font {
            itemFont = font
        }
        if let normalColor = normalColor {
            itemNormalColor = normalColor
        }
        if let selectedColor = selectedColor {
            itemSelectedColor = selectedColor
        }
        if let itemMode = itemMode {
            self.itemMode = itemMode
        }
        if let lineMode = lineMode {
            self.lineMode = lineMode
        }
        mainCollectionView.reloadData()
        setLine()
    }
    
    func select(at index: Int, animate: Bool = true) {
        if 0 <= index, index < titles.count {
            selectIndex = index
        }
        mainCollectionView.reloadData()
        setLine(animate: animate)
    }
    
    /// -1～1,负数为左，正数为右
    func itemOffset(ratio: CGFloat) {
        let absRatio = abs(ratio)
        guard absRatio < 1 else { return }
        var currentFrame = calculateLineFrame(index: selectIndex)
        let nextIndex = selectIndex + ((ratio > 0) ? 1 : -1)
        guard 0 <= nextIndex, nextIndex < titles.count else { return }
        let nextFrame = calculateLineFrame(index: nextIndex)
        let offsetX = nextFrame.origin.x - currentFrame.origin.x
        switch lineMode {
        case .full:
            let offsetWidth = nextFrame.size.width - currentFrame.size.width
            currentFrame.origin.x = currentFrame.origin.x + offsetX * absRatio
            currentFrame.size.width = currentFrame.size.width + offsetWidth * absRatio
        case .fixed:
            if absRatio < 0.5 {
                currentFrame.size.width = fixedLineWidth + offsetX * ratio * 2
                if ratio < 0 {
                    currentFrame.origin.x = currentFrame.origin.x - currentFrame.size.width + fixedLineWidth
                }
            } else {
                currentFrame.size.width = fixedLineWidth + abs(offsetX * (1 - absRatio) * 2)
                if ratio < 0 {
                    currentFrame.origin.x = nextFrame.minX
                } else {
                    currentFrame.origin.x = nextFrame.maxX - currentFrame.size.width
                }
            }
        }
        let currentCell = mainCollectionView.cellForItem(at: IndexPath(row: selectIndex, section: 0)) as? SegmentViewCell
        let nextCell = mainCollectionView.cellForItem(at: IndexPath(row: nextIndex, section: 0)) as? SegmentViewCell
        UIView.animate(withDuration: 0.1) { [weak self] in
            guard let `self` = self else { return }
            currentCell?.show(color: self.offsetColor(selectedRatio: 1 - absRatio))
            nextCell?.show(color: self.offsetColor(selectedRatio: absRatio))
            self.lineView.frame = currentFrame
        }
    }
    
    /// 0~1,越接近1为选中色，接近0为正常色
    func offsetColor(selectedRatio: CGFloat) -> UIColor {
        return (selectedRatio > 0.5) ? itemSelectedColor : itemNormalColor
    }
    
    /// 仅在滚动期间调用，注意使用
    func setIndexInScrolling(index: Int) {
        if 0 <= index, index < titles.count, selectIndex != index {
            selectIndex = index
        }
    }
}

// MARK: - 私有
private extension SegmentView {
    
    func initView() {
        addSubview(mainCollectionView)
        lineView.backgroundColor = itemSelectedColor
        mainCollectionView.addSubview(lineView)
        addSubview(dividerLine)
    }
    
    func itemWidth(string: String) -> CGFloat {
        let stringWidth = string.size(withAttributes: [NSAttributedString.Key.font:itemFont]).width
        return stringWidth
    }
    
    private func setLine(animate: Bool = false) {
        let duration = animate ? 0.3 : 0
        UIView.animate(withDuration: duration, animations: { [weak self] in
            guard let `self` = self else { return }
            self.lineView.frame = self.calculateLineFrame(index: self.selectIndex)
        }) { [weak self] _ in
            guard let `self` = self else { return }
            self.moveSelectItemIfNeed()
        }
    }
    
    func calculateLineFrame(index: Int) -> CGRect {
        guard index < titles.count else { return .zero }
        func arrangeStartX() -> CGFloat {
            var startX = itemSpace
            (0..<index).forEach { i in
                startX += (itemWidth(string: titles[i]) + itemSpace)
            }
            if lineMode == .fixed {
                startX += (itemWidth(string: titles[index]) - fixedLineWidth) / 2
            }
            return startX
        }
        switch itemMode {
        case .divide:
            let itemCount = titles.count
            let itemWidth = (bounds.width - itemSpace * CGFloat(itemCount + 1)) / CGFloat(itemCount)
            let startX = (itemWidth + itemSpace) * CGFloat(index) + itemSpace
            switch lineMode {
            case .full:
                return CGRect(x: startX, y: bounds.height - 3, width: itemWidth, height: 2)
            case .fixed:
                let offX = (itemWidth - fixedLineWidth) / 2
                return CGRect(x: startX + offX, y: bounds.height - 4, width: fixedLineWidth, height: 3)
            }
        case .arrange:
            switch self.lineMode {
            case .full:
                return CGRect(x: arrangeStartX(), y: bounds.height - 3, width: itemWidth(string: titles[index]), height: 2)
            case .fixed:
                return CGRect(x: arrangeStartX(), y: bounds.height - 4, width: fixedLineWidth, height: 3)
            }
        }
    }
    
    func moveSelectItemIfNeed() {
        guard titles.count > 0, itemMode == .arrange else { return }
        var needOffsetX = itemSpace
        (0..<selectIndex).forEach { i in
            needOffsetX += (itemWidth(string: titles[i]) + itemSpace)
        }
        var contentWidth = needOffsetX
        (selectIndex..<titles.count).forEach { i in
            contentWidth += (itemWidth(string: titles[i]) + itemSpace)
        }
        needOffsetX -= (bounds.width - itemWidth(string: titles[selectIndex]))/2
        if (needOffsetX < 0) || (contentWidth < bounds.width) {
            mainCollectionView.setContentOffset(.zero, animated: true)
        } else if (needOffsetX + bounds.width) > contentWidth {
            mainCollectionView.setContentOffset(CGPoint(x: contentWidth - bounds.width, y: 0), animated: true)
        } else {
            mainCollectionView.setContentOffset(CGPoint(x: needOffsetX, y: 0), animated: true)
        }
    }
}

// MARK: - 协议
extension SegmentView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SegmentViewCell", for: indexPath) as! SegmentViewCell
        let contentColor: UIColor = (indexPath.row == selectIndex) ? itemSelectedColor : itemNormalColor
        cell.show(content: titles[indexPath.row], font: itemFont, color: contentColor)
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemCount = titles.count
        var width: CGFloat
        switch itemMode {
        case .divide:
            width = (bounds.width - itemSpace * CGFloat(itemCount + 1)) / CGFloat(itemCount)
        case .arrange:
            width = itemWidth(string: titles[indexPath.row])
        }
        return CGSize(width: width, height: bounds.height)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpace
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: itemSpace, bottom: 0, right: itemSpace)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectIndex = indexPath.row
        setLine(animate: true)
        mainCollectionView.reloadData()
        didSelectAtIndex?(selectIndex)
    }
}
