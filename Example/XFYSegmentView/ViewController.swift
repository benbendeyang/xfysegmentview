//
//  ViewController.swift
//  XFYSegmentView
//
//  Created by leonazhu on 03/22/2019.
//  Copyright (c) 2019 leonazhu. All rights reserved.
//

import UIKit
import XFYSegmentView

class ViewController: UIViewController {
    
    @IBOutlet weak var segmentView: SegmentView!
    @IBOutlet weak var segmentView2: SegmentView!
    @IBOutlet weak var segmentView3: SegmentView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: 操作
    @IBAction func changeSliderValue(_ sender: UISlider) {
        segmentView3.itemOffset(ratio: CGFloat(sender.value))
    }
    
    @IBAction func test1(_ sender: Any) {
        segmentView.setupStyle(itemMode: .divide)
        segmentView2.setupStyle(itemMode: .divide)
        segmentView3.setupStyle(itemMode: .divide)
    }
    @IBAction func test2(_ sender: Any) {
        segmentView.setupStyle(itemMode: .arrange)
        segmentView2.setupStyle(itemMode: .arrange)
        segmentView3.setupStyle(itemMode: .arrange)
    }
    @IBAction func test3(_ sender: Any) {
        segmentView.setupStyle(lineMode: .full)
        segmentView2.setupStyle(lineMode: .full)
        segmentView3.setupStyle(lineMode: .full)
    }
    @IBAction func test4(_ sender: Any) {
        segmentView.setupStyle(lineMode: .fixed)
        segmentView2.setupStyle(lineMode: .fixed)
        segmentView3.setupStyle(lineMode: .fixed)
    }
    
    @IBAction func test5(_ sender: Any) {
        let index = Int(arc4random()%8)
        print("跳转到\(index)")
        segmentView.select(at: index)
        segmentView2.select(at: index)
        segmentView3.select(at: index)
    }
}

private extension ViewController {
    
    func initView() {
        
        segmentView.setItem(titles: ["第1选项", "第2选项", "第3选项", "第4选项"])
        segmentView2.setItem(titles: ["Aasf", "b", "assf", "kjanf"])
        segmentView3.setItem(titles: ["要长一点的选项哦哦哦哦哦哦哦哦哦哦哦", "吧", "还有", "更多", "法师佛", "老家送粉丝", "超出界面", "暗示"])
        segmentView3.selectIndexChange = { index in
            print("当前选中\(index)")
        }
        
        let segmentView4 = SegmentView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 80, width: 300, height: 40))
        segmentView4.setItem(titles: ["代码", "创建", "的", "SegmentView"])
        segmentView4.setupStyle(font: .boldSystemFont(ofSize: 12), normalColor: .gray, selectedColor: .orange, itemMode: .arrange, lineMode: .fixed)
        segmentView4.dividerLine.isHidden = true
        segmentView4.didSelectAtIndex = { index in
            print("点击了\(index)")
        }
        view.addSubview(segmentView4)
    }
}
