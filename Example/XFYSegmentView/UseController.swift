//
//  UseController.swift
//  XFYSegmentView_Example
//
//  Created by 🐑 on 2019/3/25.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import XFYSegmentView

class UseController: UIViewController {

    @IBOutlet weak var segmentView: SegmentView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    private var isDragging: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        initView()
    }
    
    // MARK: - 操作
    @IBAction func clickRightItem(_ sender: Any) {
        randomCreate()
    }
}

// MARK: - 私有
private extension UseController {
    
    func initView() {
        randomCreate()
        segmentView.setupStyle(selectedColor: .orange, itemMode: .arrange, lineMode: .fixed)
        segmentView.didSelectAtIndex = { [weak self] index in
            guard let `self` = self else { return }
            self.mainScrollView.setContentOffset(CGPoint(x: self.mainScrollView.bounds.width * CGFloat(index), y: 0), animated: true)
        }
    }
    
    func randomCreate() {
        mainScrollView.subviews.forEach { subView in
            subView.removeFromSuperview()
        }
        let count = Int(arc4random()%5) + 1
        var titles: [String] = []
        (0..<count).forEach { i in
            titles.append("第\(i)选项")
            let contentView = UIView(frame: CGRect(origin: CGPoint(x: mainScrollView.bounds.width * CGFloat(i), y: 0), size: mainScrollView.bounds.size))
            contentView.backgroundColor = UIColor.init(white: CGFloat(i)/CGFloat(count), alpha: 1)
            mainScrollView.addSubview(contentView)
        }
        mainScrollView.contentSize = CGSize(width: mainScrollView.bounds.width * CGFloat(count), height: mainScrollView.bounds.height)
        mainScrollView.setContentOffset(.zero, animated: false)
        segmentView.setItem(titles: titles)
    }
}

// MARK: - 协议
extension UseController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard isDragging else { return }
        let index = Int(scrollView.contentOffset.x / scrollView.bounds.width + 0.5)
        segmentView.setIndexInScrolling(index: index)
        let offsetX = scrollView.contentOffset.x
        let referOffsetX = scrollView.bounds.width * CGFloat(segmentView.selectIndex)
        segmentView.itemOffset(ratio: CGFloat((offsetX - referOffsetX)/scrollView.bounds.width))
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isDragging = true
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        isDragging = false
    }
}
