//
//  SegmentGroupViewController.swift
//  XFYSegmentView_Example
//
//  Created by 🐑 on 2019/3/25.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import XFYSegmentView

class SegmentGroupViewController: UIViewController {

    @IBOutlet weak var segmentGroupView: SegmentGroupView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
    
    deinit {
        print("销毁")
    }
}

// MARK: - 私有
private extension SegmentGroupViewController {
    
    func initView() {
        let view1 = UIView()
        view1.backgroundColor = .red
        let view2 = UIView()
        view2.backgroundColor = .orange
        let view3 = UIView()
        view3.backgroundColor = .gray
        let view4 = UIView()
        view4.backgroundColor = .blue
        let view5 = UIView()
        view5.backgroundColor = .yellow
        segmentGroupView.items = [
            ("选项1", view1),
            ("第二选项", view2),
            ("选项三哦哦哦哦哦哦", view3),
            ("四", view4),
            ("555555", view5)
        ]
        segmentGroupView.setupStyle(font: .systemFont(ofSize: 12), selectedColor: .orange, itemMode: .arrange, lineMode: .fixed, segmentViewHeight: 30)
        segmentGroupView.selectIndexChange = { index in
            print("第\(index)选项")
        }
    }
}
